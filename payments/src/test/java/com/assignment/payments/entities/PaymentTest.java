package com.assignment.payments.entities;

import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class PaymentTest {

    @Test()
    public void testPaymentConstructorWithParameter()
    {
        Payment payment = new Payment(1,new Date(),"Full Payment", 10.0, 1);
        assertEquals(1, payment.getId(), "id to be return from object");
        assertEquals(new Date().toString(), payment.getPaymentdate().toString(),"paymentdate to be return from object");
        assertEquals("Full Payment", payment.getType(), "type to be return from object");
        assertEquals(10.0, payment.getAmount(), "amount to be return from object");
        assertEquals(1, payment.getCustid(), "custid to be return from object");

    }

    @Test()
    public void testPaymentConstructorWithNoParameters()
    {
        Payment payment = new Payment();
        String expected  = "Payment{id=0, paymentdate=null, type='null', amount=0.0, custid=0}";
        assertEquals(expected, payment.toString(), "default values assigned to new Object ");
    }
}