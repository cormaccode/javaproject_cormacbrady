package com.assignment.payments.dao;

import com.assignment.payments.entities.Payment;

import java.util.List;

public interface IPaymentDAO {
    int rowcount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    int save(Payment payment);
}
