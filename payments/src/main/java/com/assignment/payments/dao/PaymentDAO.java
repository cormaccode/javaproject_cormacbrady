package com.assignment.payments.dao;

import com.assignment.payments.entities.Payment;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PaymentDAO implements IPaymentDAO{

    private MongoTemplate mongoTemplate;

    public PaymentDAO(MongoTemplate mongoTemplate){
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public int rowcount() {
        Query query = new Query();
        return (int) mongoTemplate.count(query, Payment.class);
    }

    @Override
    public Payment findById(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Payment payment = mongoTemplate.findOne(query, Payment.class);
        return payment;
    }

    @Override
    public List<Payment> findByType(String type) {
        Query query = new Query();
        query.addCriteria(Criteria.where("type").is(type));
        List<Payment> paymentList = mongoTemplate.find(query, Payment.class);
        return paymentList;
    }

    @Override
    public int save(Payment payment) {
        mongoTemplate.insert(payment);
        return rowcount();
    }
}
