package com.assignment.payments.rest;

import com.assignment.payments.entities.Payment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public interface IPaymentController {
    @GetMapping("/count")
    ResponseEntity<Integer> getRowCount();

    @GetMapping("/id")
    ResponseEntity<Payment> getPaymentById(@RequestParam int id);

    @GetMapping("/type")
    ResponseEntity<List<Payment>> getPaymentByType(@RequestParam String type);

    @GetMapping("/statuc")
    ResponseEntity getStatus();

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    ResponseEntity addPayment(@RequestBody Payment payment);
}
