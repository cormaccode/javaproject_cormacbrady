package com.assignment.payments.services;

import com.assignment.payments.entities.Payment;

import java.util.List;

public interface IPaymentService {
    int rowcount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    int save(Payment payment);
}
